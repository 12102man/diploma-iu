# Event clustering demo
Vladimir Ivanov, Innopolis University
Mikhail Tkachenko, Innopolis University
### Stack: Python 3.7, Neo4j

## What is this
This is a graph clustering tool. It takes articles and their annotation and returns clusters of articles that have event members in common.
Algorithm uses Neo4j (https://neo4j.com) as the graph database and Neo4j Data Science library to run the clustering algorithms.
Three algorithms can be applied: Louvain, Weakly Connected Components and Label Propagation.


## How to use?
1. Ask for dataset (EvEx dataset) (see Article section)
2. Navigate to datasets/events folder, put dataset in this folder and go to parser.py file
3. You can try all the steps from graph creation to demo JSON generation

## Dataset
In order to ask for the dataset, please email to 12102man@gmail.com
