from os import listdir
from os.path import isfile, join
from neo_sources.orm import graph
from neo_sources.models import *

TEXTS_PATH = 'wikinews20200825_texts'
LINKS_PATH = 'wikinews20200825_texts_links'

texts_files = [join(TEXTS_PATH, f) for f in listdir(TEXTS_PATH) if isfile(join(TEXTS_PATH, f))]
counter = 0


def commit_to_db(obj):
    db_article = ArticleModel()
    db_article.title = obj['title']
    db_article.url = obj['id']

    for ner in obj['ners']:
        db_ner = NERModel()
        db_ner.name = ner['word']
        db_ner.q = ner['q']
        db_ner.wiki_link = ner['link']
        graph.create(db_ner)
        db_article.ners.add(db_ner, {'start_char': ner['start_char'], 'end_char': ner['end_char']})
    graph.create(db_article)
    print(f'Article "{db_article.title}" added successfully')


for filename in texts_files:
    # Text read
    with open(filename) as f:
        text = list(
            filter(
                lambda x: x != '\n',
                [sent for sent in f]
            )
        )
        text = list(map(lambda x: x.replace('\n', ''), text))

    # Annotation read
    id = filename.split('/')[1].split('.')[0].replace('_text', '')
    position = 0
    annotations_path = join(LINKS_PATH, id + '_table.xml')
    sentences_annotations = []
    with open(annotations_path) as f:
        annotations = []
        buffer = None
        for line in f:
            splitted = line.split(',')
            word = splitted[0]
            tag = splitted[1]
            link = splitted[3]
            q = splitted[4]

            if tag == 'B' and buffer is not None and (buffer['words_count'] > 1 or buffer['link'] != ''):
                buffer['end_char'] = position
                if buffer['word'][-2] == ' "':
                    buffer['word'] = buffer['word'][:-2]
                annotations.append(buffer)
                buffer = None

            if tag == 'B':
                buffer = {'word': word, 'link': link, 'q': q, 'words_count': 1, 'start_char': position}

            if tag != 'B':
                buffer['word'] += " " + word
                buffer['words_count'] += 1

            position += len(word)

    db_object = {
        'id': id,
        'title': text[0],
        'sentences': text[1:],
        'ners': annotations,
    }
    commit_to_db(db_object)
    counter += 1
    print(f'{counter}/{len(texts_files)}')