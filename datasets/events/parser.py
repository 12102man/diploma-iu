import json
from os import listdir, close
from os.path import isfile, join
import spacy
from py2neo import Relationship

from neo_sources.models import EventModel, MemberModel, Article
from neo_sources.orm import graph

DEV_PATH = "dev"
TRAIN_PATH = "train"
ALLOWED_MEMBERS = [
    "ORGANIZATION",
    "PERSON",
    "COUNTRY",
    "CITY",
]


def parse_annotation_line(line):
    line = line.strip().replace("\n", "")
    if line.startswith("R"):
        (
            id,
            rel,
        ) = line.split("\t")
        relation, arg1, arg2 = rel.split(" ")
        return (
            "RELATION",
            id,
            relation,
            arg1.replace("Arg1:", ""),
            arg2.replace("Arg2:", ""),
        )
    else:
        id, tse, words = line.split("\t")
        tag, start, end = tse.split(" ")
        return "TAG", id, tag, start, end, words


def read_dataset(path=DEV_PATH):
    nlp = spacy.load("../../ru2_combined_400ks_96")
    print("Spacy plugin loaded...")
    events_and_members = []
    texts = [
        join(path, f)
        for f in listdir(path)
        if isfile(join(path, f)) and f.endswith(".txt")
    ]
    total = len(texts)
    print(f"total {total}")
    counter = 1
    for text in texts:
        try:
            events = {}
            persons = {}
            all = {}
            relations = {}
            textfile = open(text)
            spacytext = nlp(textfile.read())
            markup = open(text.replace(".txt", ".ann"))
            for line in markup:
                parsed_line = parse_annotation_line(line)
                if parsed_line[0] == "TAG":
                    start, end = int(parsed_line[3]), int(parsed_line[4])
                    if parsed_line[2] == "EVENT":
                        events[parsed_line[1]] = spacytext.char_span(start, end).lemma_
                    elif parsed_line[2] in ALLOWED_MEMBERS:
                        persons[parsed_line[1]] = spacytext.char_span(start, end).lemma_
                    all[parsed_line[1]] = (
                        spacytext.char_span(start, end).lemma_,
                        parsed_line[2],
                    )
                else:
                    _, _, relation, arg1, arg2 = parse_annotation_line(line)
                    if arg1 in relations:
                        relations[arg1].append((relation, arg2))
                    else:
                        relations[arg1] = [(relation, arg2)]
                    if arg2 in relations:
                        relations[arg2].append((relation, arg1))
                    else:
                        relations[arg2] = [(relation, arg1)]

            for id, evt in events.items():
                rels = []
                if id in relations:
                    rels = relations[id]
                for idx in range(len(rels)):
                    actor = rels[idx][1]
                    if actor in persons:
                        events_and_members.append((evt, rels[idx][0], persons[actor], int(text.replace('.txt', '').replace('train/', '').replace('_text', ''))))
                    # else:
                    #     rels[idx] = (rels[idx][0], all[relation_id])
        except Exception as e:
            print(text, e)
        print(f"{counter}/{total}")
        counter += 1
    return events_and_members

def add_articles(path=DEV_PATH):
    texts = [
        join(path, f)
        for f in listdir(path)
        if isfile(join(path, f)) and f.endswith(".txt")
    ]
    total = len(texts)
    print(f"total {total}")
    for filename in texts:
        with open(filename) as t:
            print(t)
            title = ''
            text = ''
            try:
                article_id = int(filename.replace('.txt', '').replace('train/', '').replace('_text', ''))
                first_line = False
                for line in t:
                    if not first_line:
                        title = line
                        first_line = True
                    else:
                        text += line
                art = Article()
                art.title = title
                art.text = text
                art.article_id = article_id
                graph.create(art)
            except Exception as e:
                print('Error occured')

def commit(evts):
    for evt in evts:
        event = EventModel.match(graph).where(name=evt[0].replace("'", "")).first()
        article_id = evt[3]
        if not event:
            event = EventModel()
            event.name = evt[0]
            graph.create(event)
        member = MemberModel.match(graph).where(name=evt[2].replace("'", "")).first()
        if not member:
            member = MemberModel()
            member.name = evt[2]
            graph.create(member)
        if not event.articles:
            event.articles = [article_id]
        else:
            event.articles.append(article_id)
            event.articles = list(set(event.articles))

        if not member.articles:
            member.articles = [article_id]
        else:
            member.articles.append(article_id)
            member.articles = list(set(member.articles))

        event.members.add(member, {"relation_type": evt[1]})
        graph.push(event)
        graph.push(member)


def update_relations(nodes):
    nodes = list(nodes)
    total = len(nodes)
    print(total)
    counter = 1
    for node in nodes:
        try:
            res = graph.run(
                f"MATCH (s: EventModel) - [:RELATED] - (:MemberModel) - [:RELATED] - (t: EventModel) where s.name = '{node.name}' RETURN t"
            )
            for r in res:
                model = EventModel.match(graph).where(name=r[0]["name"]).first()
                node.events.add(model)
                graph.push(node)
                graph.push(model)
            print(f"{counter}/{total}")
            counter += 1
        except Exception as e:
            print(e)


def update_relations_by_member(nodes):
    nodes = list(nodes)
    total = len(nodes)
    print(total)
    counter = 1
    for node in nodes:
        try:
            res = graph.run(
                f"MATCH (s: MemberModel) - [:RELATED] - (:EventModel) - [:RELATED] - (t: MemberModel) where s.name = '{node.name}' RETURN t"
            )
            for r in res:
                model = MemberModel.match(graph).where(name=r[0]["name"]).first()
                node.members.add(model)
                graph.push(node)
                graph.push(model)
            print(f"{counter}/{total}")
            counter += 1
        except Exception as e:
            print(e)


def find_possible_values(graph, label_name):
    nodes = MemberModel.match(graph).all()
    return list(sorted(set(map(lambda x: getattr(x, label_name), nodes))))

def calculate_graph_connectivity(graph, method):
    clusters = find_possible_values(graph, method)
    cluster_statistics = {}
    for c in clusters:
        hopcounts = []
        print(f'Cluster #{c}')
        nodes = MemberModel.match(graph).where(**{method: c}).all()
        for a in nodes:
            for b in nodes:
                if a != b:
                    res = graph.evaluate(f"match p = shortestPath((m)-[:MEMBER_RELATION*..100]-(n)) where n.name = '{a.name}' and m.name = '{b.name}' return length(p)")
                    hopcounts.append(res)
        if len(hopcounts) > 0:
            cluster_statistics[c] = {
                'count': len(nodes),
                'hops': len(hopcounts),
                'av_hopcount': sum(hopcounts) / len(hopcounts)
            }
            print(cluster_statistics[c])
    with open(f'{method}_hops.json', 'w') as outfile:
        json.dump(cluster_statistics, outfile)


def calculate_graph_connectivityv2(graph, method):
    clusters = find_possible_values(graph, method)
    cluster_statistics = {}
    relation = Relationship.type("MEMBER_RELATION")
    for c in clusters:
        inter = 0
        intra = 0
        print(f'Cluster #{c}')
        cluster_nodes = MemberModel.match(graph).where(**{method: c}).all()
        for n in cluster_nodes:
            for r in n.members:
                if r in cluster_nodes:
                    inter += 1
                else:
                    intra += 1
        if intra != 0 and inter != 0:
            cluster_statistics[c] = {
                'count': len(cluster_nodes),
                'inter': inter,
                'intra': intra,
                'avg': inter / intra if intra != 0 else 0
            }
    with open(f'{method}_interintra.json', 'w') as outfile:
        json.dump(cluster_statistics, outfile)


def create_demo_json(graph, method, path=DEV_PATH):
    clusters = find_possible_values(graph, method)
    cluster_statistics = []
    for c in clusters:
        print(f'Cluster #{c}')
        cluster_nodes = MemberModel.match(graph).where(**{method: c}).all()
        articles = []
        for n in cluster_nodes:
            articles.extend(n.articles)
        articles = list(set(articles))
        articles_with_texts = []
        for article in articles:
            with open(join(path, f'{article}_text.txt')) as f:
                articles_with_texts.append({
                    'id': article,
                    'text': f.read()
                })
        cluster_statistics.append({
            'articles': articles_with_texts,
            'nodes': list(map(lambda x: x.name, cluster_nodes))
        })

    with open(f'{method}_demo.json', 'w') as outfile:
        json.dump(cluster_statistics, outfile, ensure_ascii=False)

if __name__ == "__main__":
    create_demo_json(graph, 'louvainId', TRAIN_PATH)
    # events_and_members = read_dataset(TRAIN_PATH)
    # print(events_and_members)
    # commit(events_and_members)
    # print("updating relations")
    # print(len(EventModel.match(graph)))
    # update_relations_by_member(MemberModel.match(graph))
    # calculate_graph_connectivity(graph, 'louvainId')
    # calculate_graph_connectivityv2(graph, 'louvainId')
    #
    # calculate_graph_connectivity(graph, 'lpId')
    # calculate_graph_connectivityv2(graph, 'lpId')

