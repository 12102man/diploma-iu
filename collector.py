import json

import feedparser
from goose3 import Goose

FEED_LINK = 'http://feeds.feedburner.com/RuWikinewsLatestNews'


if __name__=='__main__':
	articles = feedparser.parse('http://feeds.feedburner.com/RuWikinewsLatestNews').entries
	parsed_articles = []
	for article in articles:
		parsed_article = Goose({}).extract(article.link)
		parsed_articles.append({
			'title': parsed_article.title,
			'text': parsed_article.cleaned_text,
			'link': parsed_article.canonical_link,
		})
	with open('dataset.json', 'w') as f:
		f.write(json.dumps({'articles': parsed_articles}))

