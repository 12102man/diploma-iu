from goose3 import Goose
from py2neo import Graph
import spacy
import feedparser

from neo_sources.models import ArticleModel, SentenceModel, NERModel

graph = Graph("bolt://217.28.226.174//:7687", auth=("neo4j", "twist-proxy-middle-logic-rover-7012"))
g = Goose()


def add_article(url):
    article = g.extract(url=url)
    nlp = spacy.load("en_core_web_sm")
    doc = nlp(article.cleaned_text)

    db_article = ArticleModel()
    db_article.url = url
    db_article.title = article.title

    for s in doc.sents:
        sent = SentenceModel()
        sent.text = s.text
        for e in s.ents:
            ner = NERModel()
            ner.name = e.text
            ner.label = e.label_
            graph.create(ner)
            sent.ners.add(ner, {'start_char': e.start_char, 'end_char': e.end_char})
        graph.create(sent)
        db_article.sentences.add(sent)
    graph.create(db_article)
    print(f'Article "{article.title}" added successfully')


# add_article("https://www.npr.org/sections/live-updates-2020-election-results/2020/11/07/932062684/far-from-over-trump-refuses-to-concede-as-ap-others-call-election-for-biden")

# feed = feedparser.parse('http://rssfeeds.usatoday.com/UsatodaycomNation-TopStories')
# for ent in feed.entries:
#     add_article(ent.link)