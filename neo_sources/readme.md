# Queries

#### Get all ners and a list of articles where they are met:
```MATCH (ner:NERModel)-[:IS_IN|IS_PART_OF*2]-(article:ArticleModel) RETURN ner.name,collect(DISTINCT article.title)```

#### Get number or articles where ner is met
```match (n:NERModel) where size((n:NERModel)-[:IS_IN|IS_PART_OF*2]-()) = 1 return count(n)```



# Analysis

### Experiment 1: take two articles on same topic and see their connectivity

Connectivity: number of NERS met in all articles of a set divided by a total number of unique NERs detected in all articles

Articles: https://en.wikinews.org/wiki/Philippines_Supreme_Court_to_probe_leak_of_draft_judgment_in_election_case, https://en.wikinews.org/wiki/Philippines_Senate_and_House_of_Representatives_to_probe_%E2%80%98P50-million_drug_bribe_mess%E2%80%99
Connectivity: 180 NERs out of 225 all