from py2neo.ogm import Model, Property, RelatedFrom, Related


class ArticleModel(Model):
    __primarykey__ = "title"

    title = Property()
    url = Property()
    sentences = RelatedFrom("SentenceModel", "IS_PART_OF")
    ners = RelatedFrom("NERModel", "IS_IN")


class SentenceModel(Model):
    __primarykey__ = "text"
    text = Property()


class NERModel(Model):
    __primarykey__ = "q"

    name = Property()
    type = Property()
    label = Property()
    q = Property()
    wiki_link = Property()


class EventModel(Model):
    __primarykey__ = "name"
    name = Property()
    members = RelatedFrom("MemberModel", "RELATED")
    events = Related("EventModel", "EVENT")
    articles = Property()


class Article(Model):
    __primarykey__ = "article_id"
    article_id = Property()
    title = Property()
    text = Property()


class MemberModel(Model):
    __primarykey__ = "name"

    name = Property()
    members = Related("MemberModel", "MEMBER_RELATION")
    louvainId = Property()
    lpId = Property()
    articles = Property()
